module C = Configurator.V1

let is_x86_64 = Config.hw_is_x86_64 ()

let sixtyfour = Sys.word_size = 64

let symbols =
  [
    (if is_x86_64 then Some ("USE_ASM_X86_64", None) else None);
    Some ("SECP256K1_INLINE", Some "inline");
    Some ("SECP256K1_RESTRICT", Some "restrict");
    Some ("SECP256K1_TAG_PUBKEY_EVEN", Some "0x02");
    Some ("SECP256K1_TAG_PUBKEY_ODD", Some "0x03");
    Some ("SECP256K1_TAG_PUBKEY_UNCOMPRESSED", Some "0x04");
    Some ("SECP256K1_TAG_PUBKEY_HYBRID_EVEN", Some "0x06");
    Some ("SECP256K1_TAG_PUBKEY_HYBRID_ODD", Some "0x07");
    Some ("ENABLE_MODULE_RECOVERY", None);
    Some ("ECMULT_WINDOW_SIZE", Some "15");
    Some ("ECMULT_GEN_PREC_BITS", Some "4");
  ]

let generate_defines symbols =
  let gen_symbol sym =
    match sym with
    | None -> None
    | Some (sym, None) -> Some (Printf.sprintf "-D%s" sym)
    | Some (sym, Some def) -> Some (Printf.sprintf "-D%s=%s" sym def)
  in
  List.filter_map gen_symbol symbols

let defines = generate_defines symbols

let get_config c =
  let open C.Pkg_config in
  let default = {libs = ["-lgmp"]; cflags = []} in
  let pkg_conf =
    Option.value
      (Option.bind (get c) (fun pc -> query pc ~package:"gmp"))
      ~default
  in
  {libs = pkg_conf.libs; cflags = List.append pkg_conf.cflags defines}

let () =
  C.main ~name:"discover" (fun c ->
      let conf = get_config c in
      C.Flags.write_sexp "c_flags.sexp" conf.cflags ;
      C.Flags.write_sexp "c_library_flags.sexp" conf.libs)
