# Build the wasm file / publish on npm

## Setup npm

```
  npm config set @nomadic-labs:registry https://gitlab.com/api/v4/projects/25020679/packages/npm/
  npm config set -- '//gitlab.com/api/v4/projects/25020679/packages/npm/:_authToken' "<your_token>"
```

## Install emsdk

See https://emscripten.org/docs/getting_started/downloads.html

## Build wasm

```
  make dist
```

## Test

```
  npm install dist/
  dune build @runtest-js
```

## Publish

```
  cd dist
  ;; bump version number in package.json
  npm publish
```